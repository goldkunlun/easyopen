package com.gitee.easyopen.doc;

public enum DataType {
    BYTE("byte"), SHORT("short"), INT("integer"), LONG("long"), FLOAT("float"), DOUBLE("double")
    , CHAR("char"), BOOLEAN("boolean"), ARRAY("array"), OBJECT("object"),STRING("string"),UNKNOW("");
    
    DataType(String v) {
        val = v;
    }
    
    private String val;
    
    public String getValue() {
        return this.val;
    }
}
