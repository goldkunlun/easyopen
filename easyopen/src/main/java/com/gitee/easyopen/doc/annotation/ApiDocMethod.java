package com.gitee.easyopen.doc.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface ApiDocMethod {
    /**
     * 描述，接口简单描述，如：用户登录，发送验证码。支持html标签
     */
    String description() default "";
    
    /**
     * 备注，可写更详细的介绍，支持html标签
     * @return
     */
    String remark() default "";

    /**
     * 自定义参数
     */
    ApiDocField[] params() default {};

    /**
     * 指定参数类型
     */
    Class<?> paramClass() default Object.class;

    /**
     * 自定义返回参数
     */
    ApiDocField[] results() default {};

    /**
     * 指定返回参数
     */
    Class<?> resultClass() default Object.class;
    
    /**
     * 数组元素class类型
     * @return
     */
    Class<?> elementClass() default Object.class;
}
