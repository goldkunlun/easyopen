package com.gitee.easyopen.doc;

import java.util.List;

public class ParamHtmlBuilder {
    
    public String buildHtml(ApiDocFieldDefinition definition) {
        StringBuilder html = new StringBuilder();
        html.append("<tr>")
            .append("<td>"+definition.getName()+"</td>")
            .append("<td>"+definition.getDataType()+"</td>")
            .append("<td>"+this.getRequireHtml(definition)+"</td>")
            .append("<td>"+buildExample(definition)+"</td>")
            .append("<td>"+definition.getDescription()+"</td>");
        html.append("</tr>");
        
        return html.toString();
    }
    
    protected String buildExample(ApiDocFieldDefinition definition) {
        StringBuilder html = new StringBuilder();
        if(definition.getElements().size() > 0) {
            html.append("<table>")
                .append("<tr>")
                    .append("<th width=\"25%\">名称</th>")
                    .append("<th width=\"15%\">类型</th>")
                    .append("<th width=\"5%\">是否必须</th>")
                    .append("<th width=\"15%\">示例值</th>")
                    .append("<th width=\"40%\">描述</th>")
                .append("</tr>");
            
            List<ApiDocFieldDefinition> els = definition.getElements();
            for (ApiDocFieldDefinition apiDocFieldDefinition : els) {
                html.append("<tr>")
                    .append("<td>"+apiDocFieldDefinition.getName()+"</td>")
                    .append("<td>"+apiDocFieldDefinition.getDataType()+"</td>")
                    .append("<td>"+this.getRequireHtml(definition)+"</td>")
                    .append("<td>"+buildExample(apiDocFieldDefinition)+"</td>")
                    .append("<td>"+apiDocFieldDefinition.getDescription()+"</td>")
                .append("</tr>");
            }
            html.append("</table>");
        }else{
            html.append(buildExampleValue(definition));
        }
        return html.toString();
    }
    
    private String getRequireHtml(ApiDocFieldDefinition definition) {
        if("true".equals(definition.getRequired())) {
            return "<strong>是</strong>";
        }else{
            return "否";
        }
    }

    protected String buildExampleValue(ApiDocFieldDefinition definition) {
        return "<input type=\"text\" name=\"" + definition.getName() + "\" value=\"" + definition.getExample() + "\"/>";
    }
}
