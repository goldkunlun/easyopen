package com.gitee.easyopen.message;

/**
 *
 */
public interface Errors {

    String openIsv = "open.error_";

    ErrorMeta SUCCESS = new ErrorMeta(openIsv, "0", "success");
    ErrorMeta SYS_ERROR = new ErrorMeta(openIsv, "-9", "系统错误");

    /** 调用不存在的服务请求:{0} */
    ErrorMeta NO_API = new ErrorMeta(openIsv, "1", "不存在的服务请求");
    /** 服务请求({0})的参数非法 */
    ErrorMeta ERROR_PARAM = new ErrorMeta(openIsv, "2", "参数非法");
    /** 服务请求({0})缺少应用键参数:{1} */
    ErrorMeta NO_APP_ID = new ErrorMeta(openIsv, "3", "缺少应用键参数");
    /** 服务请求({0})的应用键参数{1}无效 */
    ErrorMeta ERROR_APP_ID = new ErrorMeta(openIsv, "4", "应用键参数无效");
    /** 服务请求({0})需要签名,缺少签名参数:{1} */
    ErrorMeta NO_SIGN_PARAM = new ErrorMeta(openIsv, "5", "缺少签名参数");
    /** 服务请求({0})的签名无效 */
    ErrorMeta ERROR_SIGN = new ErrorMeta(openIsv, "6", "签名无效");
    /** 超时 */
    ErrorMeta TIMEOUT = new ErrorMeta(openIsv, "7", "业务逻辑出错");
    /** 服务请求({0})业务逻辑出错 */
    ErrorMeta ERROR_BUSI = new ErrorMeta(openIsv, "8", "业务逻辑出错");
    /** 服务不可用 */
    ErrorMeta SERVICE_INVALID = new ErrorMeta(openIsv, "9", "服务不可用");

    ErrorMeta TIME_INVALID = new ErrorMeta(openIsv, "10", "请求时间格式错误");

    ErrorMeta NO_FORMATTER = new ErrorMeta(openIsv, "11", "序列化格式不存在");
    
    ErrorMeta NO_CONTECT_TYPE_SUPPORT = new ErrorMeta(openIsv, "12", "不支持contectType");
    
    ErrorMeta ERROR_JSON_DATA = new ErrorMeta(openIsv, "13", "json格式错误");
    
    ErrorMeta ERROR_ACCESS_TOKEN = new ErrorMeta(openIsv, "14", "accessToken错误");
    ErrorMeta EXPIRED_ACCESS_TOKEN = new ErrorMeta(openIsv, "15", "accessToken expired");
    ErrorMeta UNSET_ACCESS_TOKEN = new ErrorMeta(openIsv, "16", "accessToken not found");
    
    ErrorMeta ERROR_OPT_JWT = new ErrorMeta(openIsv, "17", "jwt操作失败");
    ErrorMeta ERROR_JWT = new ErrorMeta(openIsv, "18", "jwt错误");
    ErrorMeta ERROR_ALGORITHM = new ErrorMeta(openIsv, "19", "算法不支持");
    
    ErrorMeta ERROR_SSL = new ErrorMeta(openIsv, "20", "ssl交互错误");
    
    ErrorMeta BUSI_PARAM_ERROR = new ErrorMeta("100", "业务参数错误");

}
