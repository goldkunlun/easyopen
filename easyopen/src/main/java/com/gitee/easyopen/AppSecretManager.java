package com.gitee.easyopen;

import java.util.Map;

public interface AppSecretManager {
    
    /**
     * 初始化秘钥数据
     * @param appSecretStore
     */
    void addAppSecret(Map<String,String> appSecretStore);

    /**
     * 获取应用程序的密钥
     *
     * @param appKey
     * @return
     */
    String getSecret(String appKey);

    /**
     * 是否是合法的appKey
     *
     * @param appKey
     * @return
     */
    boolean isValidAppKey(String appKey);
}