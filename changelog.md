# 更新日志


## 1.6.1

- 修复文档结果显示BUG

## 1.6.0

- 新增oauth2的refreshToken功能

## 1.5.4

- 优化文档显示

## 1.5.3

- 优化ErrorFactory.getErrorMessage()

## 1.5.2

- 修复getRequest()空指针异常

## 1.5.1

- 修复RedisSession无法设置sessionTimeout问题

## 1.5.0 

- 【新增】@ApiDocMethod注解新增remark属性，方便对接口进行详细说明
- 【优化】优化doc模板

## 1.4.6~1.4.10

- 6 方法扩展
- 7 优化RedisSession
- 8 修复throw Exception前端显示问题
- 9 优化doc模板
- 10 优化RedisSessionManager


## 1.4.5

- 【新增】在ApiContext新增方法方便调用

## 1.4.2~1.4.4

- 【优化】优化doc文档

## 1.4.1

- 【修复】修复@ApiDocField注解指定elementClass失效问题

## 1.4.0

- 【新增】新增RSA+AES数据加密交互模式(数据加密传输)
- 【新增】SDK客户端支持session交互
- 【新增】可自定义默认版本号,见ApiConfig
- 【新增】新增自定义session管理,支持redis
- 【修复】修复server在有contextPath的情况下文档页面路径问题

## 1.3.1 

- 【修复】修复拦截器为空BUG

## 1.3.0

- 【新增】新增拦截器ApiInterceptor，原理同springmvc拦截器
- 【新增】签名校验新增RSA校验，校验规则见：签名算法.txt (有问题,不推荐用,1.4.0版本会修复)

## 1.2.1

- 【修复】修复cglib引起的BUG
- 【优化】添加Signer，签名算法可自定义实现

## 1.2.0

- 【新增】支持JWT
- 【新增】新增oauth2认证


## 1.0.3

- 【优化】调整文档页面样式

## 1.0.2

- 【优化】代码优化，完善注释

