package com.gitee.easyopen.sdk;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.apache.commons.io.IOUtils;

import com.gitee.easyopen.sdk.util.MD5Util;


public class SdkContext {

    private static String sessionid;
    private static String randomKey = MD5Util.encrypt16(UUID.randomUUID().toString());
    // 公钥
    private static String publicKey = "";
    
    static {
        InputStream in = SdkContext.class.getClassLoader().getResourceAsStream("pub.key");
        try {
            publicKey = IOUtils.toString(in, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static String getSessionId() {
        return sessionid;
    }

    public static void setSessionId(String id) {
        sessionid = id;
    }

    public static String getRandomKey() {
        return randomKey;
    }

    public static String getPublicKey() {
        return publicKey;
    }

    public static void setPublicKey(String publicKey) {
        SdkContext.publicKey = publicKey;
    }

}
