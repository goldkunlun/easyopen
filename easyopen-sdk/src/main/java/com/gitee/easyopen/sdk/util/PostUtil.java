package com.gitee.easyopen.sdk.util;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class PostUtil {

    private static final String UTF8 = "UTF-8";

    private static final String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";
    private static final String CONTENT_TYPE_TEXT = "text/plain;charset=UTF-8";
    private static final String EMPTY_DATA = "{}";

    public static String postString(String url, JSONObject params) throws Exception {
        CloseableHttpResponse response = post(url, params);
        String resp = EntityUtils.toString(response.getEntity());
        response.close();
        return resp;
    }
    
    public static CloseableHttpResponse post(String url, JSONObject params) throws Exception {
        return post(url, params, null, null);
    }
    
    /**
     * 发送普通文本
     * @param url
     * @param content
     * @return
     * @throws Exception
     */
    public static CloseableHttpResponse post(String url, String content) throws Exception {
        return post(url, content, null, null);
    }

    public static CloseableHttpResponse post(String url, Object params, Map<String, String> header, CookieStore cookieStore)
            throws Exception {

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();

        try {
            HttpPost httpPost = new HttpPost(url);
            
            bindHeader(httpPost, header);
            
            String requestBody = null;
            String contentType = null;
            
            if(params instanceof String) {
                requestBody = (String)params;
                contentType = CONTENT_TYPE_TEXT;
            } else {
                contentType = CONTENT_TYPE_JSON;
                requestBody = params == null ? EMPTY_DATA : (JSON.toJSONString(params));
            }
             
            // 设置请求体
            StringEntity stringEntity = new StringEntity(requestBody, UTF8);
            stringEntity.setContentEncoding(UTF8);
            stringEntity.setContentType(contentType);

            httpPost.setEntity(stringEntity);

            return httpClient.execute(httpPost);
        } catch (Exception e) {
            throw e;
        }
    }
       
    
    private static void bindHeader(HttpPost httpPost ,Map<String, String> header ) {
        if(header != null) {
            // 设置header
            Set<Entry<String, String>> headerEntry = header.entrySet();
            for (Entry<String, String> entry : headerEntry) {
                httpPost.addHeader(entry.getKey(), entry.getValue());
            }
        }
    }
    

}
