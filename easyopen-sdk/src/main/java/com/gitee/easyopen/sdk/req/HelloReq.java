package com.gitee.easyopen.sdk.req;

import com.gitee.easyopen.sdk.BaseNoParamReq;
import com.gitee.easyopen.sdk.resp.HelloResp;

public class HelloReq extends BaseNoParamReq<HelloResp> {

    public HelloReq(String name) {
        super(name);
    }


}